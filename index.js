let emptyObj = new Object();
emptyObj.propertie1 = "This is string";
emptyObj["12propertie"] = 12345;
let a = "propertie3"
emptyObj[a] = {
    abg: "abg"
};
delete emptyObj.propertie1;
console.log("emptyObj: ", emptyObj);
console.log("undefinedPropetie: ", emptyObj.undefinedPropetie);
console.log("\nHmmm, what are in my emptyObject?");
for (let prop in emptyObj) {
    console.log(prop, ": ", emptyObj[prop]);
}
console.log("Like mas: ", Object.keys(emptyObj));
console.log("Like mas: ", Object.getOwnPropertyNames(emptyObj));

function FuncObj(pr1, pr2, pr3) {
    this.pr1 = pr1;
    this.pr2 = pr2;
    this.pr3 = pr3;
}
let o1 = new FuncObj(1, 2, 3);
let o2 = new FuncObj("rere", "erer", "pokolopo");
console.log(o1)
console.log(o2)
let r = {
    a: 1,
    b: 2,
    getAB: function () {
        console.log(this.a + ', ' + this.b);
    }
}
let o3 = Object.create(r);
o3.a = 4;
o3.b = 5;
o3.getAB();
FuncObj.prototype.newPropertie = null;
o2.newPropertie = "po";
console.log("Like mas: ", Object.getOwnPropertyNames(o2));
console.log("Like mas: ", Object.getPrototypeOf(o2));
let getSet = {
    a: 4,
    get b() { return this.a + 10 },
    set c(x) { this.a = x }
}
console.log("getSet val: ", getSet.a);
console.log("getSet get+10: ", getSet.b);
getSet.c = 16;
console.log("getSet set16: ", getSet.a);

const ObjMas = [];
for (let i = 0; i < 10; i++) {
    ObjMas.push({ num: Math.round(Math.random() * 1000), place: i })
}
ObjMas.length = 7;
ObjMas.sort((a, b) => { return b.num - a.num })
console.log(ObjMas)
console.log(ObjMas.filter((a) => { return a.num % 2 == 1 }))

